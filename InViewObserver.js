import React from 'react';
import PropTypes from 'prop-types';

/**
 * InViewObserver
 * @description [description]
 * @example
  <div id="InViewObserver"></div>
  <script>
	ReactDOM.render(React.createElement(Components.InViewObserver, {
	}), document.getElementById("InViewObserver"));
  </script>
 */
class InViewObserver extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'inview-observer';
	}

	componentDidMount() {
		const {onInView, isActive} = this.props;

		if (!isActive) {
			return;
		}

		if (!SERVER) {
			if (onInView) {
				this.component.onInView = () => {
					onInView();
				};
			}

			if (typeof window === 'object') {
				if (window.IntersectionObserver) {
					if (!window._blockObserver) {
						const onIntersection = entries => {
							entries.forEach(item => {
								if (item.intersectionRatio > 0) {
									// TODO: check if is within a carousel... if so, check if it's selected
									/*
									const isInCarousel = Dom.closest(item.target, '.slider__cell');
									const canActivate = (isInCarousel && isInCarousel.classList.contains('is-selected')) || !isInCarousel;
									*/

									if (item.target.onInView) {
										item.target.onInView.call();
									}

									window._blockObserver.unobserve(item.target);
								}
							});
						};

						const obSettings = {
							rootMargin: '-200px 0px',
							threshold: 0.01
						};

						window._blockObserver = new IntersectionObserver(onIntersection, obSettings);
					}

					window._blockObserver.observe(this.component);
				} else if (onInView) {
					onInView();
				}
			}
		}
	}

	render() {
		const {children, className} = this.props;

		const atts = {};

		if (className) {
			atts.className = className;
		}

		return (
			<div {...atts} ref={component => (this.component = component)}>
				{children}
			</div>
		);
	}
}

InViewObserver.defaultProps = {
	isActive: true,
	onInView: null,
	children: '',
	className: ''
};

InViewObserver.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
	isActive: PropTypes.bool,
	onInView: PropTypes.func
};

export default InViewObserver;
